\chapter{Introduction}
\label{ch:intro}

\section{Introduction}

The United States Air Force (USAF) now operates two main platforms of strategic transport aircraft, the C-17 and the C-5M. Both aircraft are considered fully developed and mature platforms. But whereas the C-17 hasn't changed much from its basic configuration, the C-5M version has undergone a thorough redesign and refurbishment that makes use of 52 previous airframes. Even though both models are still viable today and will continue to be so far into the 2040s and beyond, it is evident that they must be replaced with more sophisticated versions.

Both of the current aircraft are designed with the primary goal of allowing rapid global mobility, which will allow forces to be rapidly mobilized and maintained anywhere in the world. This includes using the entire range of capabilities provided by each platform to move soldiers, artillery, armor, and support equipment. The upcoming heavy-lift aircraft (HLA) generation needs to be in line with these international demands, which means that it needs to have enough capacity to quickly accumulate substantial assets and long-range capabilities appropriate for operations in the Pacific.

High performance standards are outlined in the new HLA's Request for Proposal (RFP). A payload of 430,000 lbs must be delivered by the aircraft over a minimum unrefueled range of 2,500 nm; a reduced payload of 295,000 lbs may be delivered over a maximum 5,000 nm distance. Notably, the HLA is anticipated to carry up to three M-1 Abrams Main Battle Tanks (MBTs) at once, surpassing the capability of the C-5M. Furthermore, as has been the case in the past with the C-5 class, direct entrance into hazard zones must be avoided; hence, longer, paved runways must be assumed for operational safety.

Another crucial need is enhanced self-sufficiency, which calls for effective loading and unloading capabilities at forward deployment locations. To emulate the C-5's simplified ground operations, as little ground equipment as possible should be required, with low main deck ground clearance being especially important. Additionally, in order to guarantee the aircraft's compliance with the current infrastructure, its total dimensions must meet the requirements of major ICAO class F airports, which allow for an aircraft's limited span of 80 meters while parked.

The suggested HLA will make use of current engines from the military or the commercial transport industry, utilizing tried-and-true technology to maximize efficiency and dependability. Production of the new type is expected to comprise 160 units for the USAF and its military allies, and a further 20 units designated for sales in specific niche commercial markets, leveraging its distinctive outsized cargo capacities.

\section{Market Analysis} % BLM SELESAI

The market analysis for the BUING aircraft entails a thorough examination of the prevailing market conditions, demand projections, competitive landscape, and strategic imperatives within the dynamic military cargo aircraft sector. This analysis serves as a foundational framework for understanding market dynamics, identifying growth opportunities and formulating strategic initiatives to position the BUING aircraft effectively in the competitive marketplace.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{Performance/COMPARATIVE-PAYLOAD.pdf}
	\caption{Payload capacity against range for similar aircraft.}
	\label{fig:payload-vs-range}
\end{figure}

\subsection{Analysis of Comparative Aircraft Specifications}

The given Fig.~\ref{fig:payload-vs-range} shows that the aircraft that most closely match the specifications are located within the range that the AN-124 and AN-225 models indicate. Unexpectedly, these aircraft have payload and range limits of 77,519 kg to 250,000 kg and 4,445 km to 15,000 km,respectively. These values that have been seen align with the specifications specified in the Request for Proposal (RFP). As a result, locating such aircraft offers an ideal opportunity to close the current capability gap and maybe replace the functions that the C-17 and C-5M aircraft currently perform with the intended BUING aircraft.

\subsection{Current Market Condition}

The military cargo aircraft market operates within a complex and ever-evolving global landscape characterized by geopolitical uncertainties, shifting defense priorities, and technological advancements. With diverse operational requirements ranging from tactical airlift missions to strategic logistics support, military cargo aircraft play a pivotal role in facilitating rapid deployment, humanitarian assistance, and disaster relief efforts worldwide. Major industry players, including Lockheed Martin, Boeing, and Airbus
Defense $\&$ Space, continually innovate to meet the evolving needs of defense agencies and adapt to emerging trends shaping the market. Recent developments underscore the increasing emphasis on sustainability, digitalization, and multi-mission capabilities within the military cargo aircraft sector. As defense agencies seek to enhance operational efficiency, reduce environmental footprint, and maximize mission flexibility, manufacturers are tasked with delivering innovative solutions that offer enhanced performance, reliability, and cost-effectiveness. Furthermore, the growing integration of unmanned aerial systems (UAS) and autonomous technologies presents both challenges and opportunities for traditional cargo aircraft platforms, necessitating agile adaptation and strategic foresight to remain
competitive in the rapidly evolving market landscape.

\subsection{Demand Projections}

Projections for the demand of military cargo aircraft over the next decade reflect a mix of geopolitical realities, defense modernization initiatives, and operational requirements across various theaters of operation. While precise demand figures may fluctuate in response to geopolitical developments, defense budget allocations, and emergent threats, industry forecasts indicate a sustained need for strategic airlift capabilities to support expeditionary operations, peacekeeping missions, and humanitarian endeavors. The projected demand for the BUING aircraft is estimated to range between 2 to 4 units over the forecast period, with potential variations influenced by regional security dynamics, coalition partnerships, and technological advancements.

\subsection{Competitive Landscape}

The competitive landscape of the military cargo aircraft market is characterized by intense rivalry among industry incumbents vying for market share, contract opportunities, and technological leadership. Established manufacturers leverage their extensive experience, engineering expertise, and global supply chain networks to deliver cutting-edge solutions that meet the diverse needs of defense customers. The BUING aircraft aims to carve out a distinct niche in the market by offering superior performance capabilities, operational flexibility, and cost-effectiveness compared to existing platforms. By focusing on
innovation, customer-centric design, and strategic partnerships, the BUING seeks to position itself as a preferred choice for defense agencies seeking reliable and versatile transport solutions tailored to their mission requirements.

\subsection{Purpose Built Military Cargo Aircraft}

Military cargo aircraft serve a crucial purpose in facilitating the rapid and efficient transport of personnel, equipment, supplies, and humanitarian aid worldwide. With strategic mobility capabilities, they enable defense agencies to project power, deter aggression, and support allied nations in times of crisis or conflict. These aircraft play a pivotal role in tactical airlift operations, delivering troops, vehicles, and cargo directly to theaters of operation to sustain combat missions, peacekeeping efforts, and humanitarian relief. Additionally, they provide logistical support by transporting critical supplies and
equipment to forward operating bases and combat zones, ensuring the continuous flow of resources necessary for operational readiness. Military cargo aircraft also contribute to humanitarian assistance and disaster relief by airlifting relief supplies and medical equipment to affected areas, facilitating rapid response and coordination among relief organizations and military forces. Their strategic airlift capabilities further enable the swift deployment of forces in response to emerging threats, crises, or contingencies, enhancing national and international security. Overall, military cargo aircraft are
indispensable assets, providing versatility, reliability, and operational capabilities to address a wide range of security challenges and humanitarian crises effectively.

\subsection{Market Forecast for Military Cargo Aircraft (2022-2035)}
\label{sec:market-forecast}




The military cargo aircraft market is poised for steady growth and significant opportunities from 2022 to 2035. Demand for these aircraft is expected to rise steadily, fueled by factors such as geopolitical tensions, defense modernization initiatives, and increased requirements for strategic airlift capabilities worldwide. Emerging security challenges, regional conflicts, and peacekeeping operations will drive the need for rapid mobility and logistical support, spurring demand for modernized and versatile transport solutions. Manufacturers are anticipated to invest in technological advancements to enhance aircraft performance, fuel efficiency, and mission capabilities. Integration of advanced materials, autonomous
technologies, and digitalization initiatives will further bolster aircraft reliability, maintainability, and operational readiness.

The competitive landscape of the military cargo aircraft market is expected to remain dynamic, with established manufacturers and emerging players vying for market share and contract opportunities. Product innovation, performance capabilities, and cost-effectiveness will be crucial for manufacturers to differentiate themselves and secure long-term partnerships with defense agencies and commercial operators. Close adherence to regulatory requirements, including FAA certification standards and
international aviation regulations, will be essential for market entry and operational success. Manufacturers that can adapt to changing market dynamics, leverage technological advancements, and meet customer requirements effectively will be well-positioned to capitalize on emerging opportunities and drive market expansion in the coming years.

The market analysis for the HLA-921 aircraft entails a thorough examination of the prevailing market
conditions, demand projections, competitive landscape, and strategic imperatives within the dynamic
military cargo aircraft sector. This analysis serves as a foundational framework for understanding the
market dynamics, identifying growth opportunities, and formulating strategic initiatives to position the
HLA-921 aircraft effectively in the competitive marketplace.x

\section{Design Requirements and Objectives}

\begin{sidewaystable}[ht!]
	\centering
	\caption{Table of Requirements and Compliance (RFP)}
	\begin{tabular}{p{0.2\textwidth}|| p{0.2\textwidth}|| p{0.25\textwidth}|| p{0.1\textwidth}|| >{\raggedright\arraybackslash} p{0.1\textwidth}}
		\toprule
		Description                 & RFP Requirement                                                                            & BUING                                                                                    & Met        & Section                                               \\ \midrule

		Aircraft EIS                & 2033                                                                                       & 2033                                                                                     & \checkmark & \ref{sec:market-forecast}                             \\

		Engine EIS                  & Within 5 Years                                                                             & 2006                                                                                     & \checkmark & \ref{sec:engine-selection}                            \\

		HLA Configuration           & Fixed Wing                                                                                 & Fixed Wing                                                                               & \checkmark & \ref{sec:wing-design}, \ref{sec:high-lift-devices}    \\

		Payload                     & 430,000 lbs                                                                                & 482,400                                                                                  & \checkmark & \ref{sec:payload-range}                               \\

		Payload Type                & \begin{itemize}
			                              \item 3 Tanks
			                              \item 48 Pallets
			                              \item 430 Passengers
		                              \end{itemize}                                                                       & \begin{itemize}
			                                                                                                                    \item 3 Tanks
			                                                                                                                    \item 48 Pallets
			                                                                                                                    \item 449 Passengers
		                                                                                                                    \end{itemize}                                                                     & \checkmark & \ref{sec:payload-arrangement}                                       \\

		Crew                        & \begin{itemize}
			                              \item 1 Pilot
			                              \item 1 Co-pilot
			                              \item 2 Loadmasters
			                              \item 4 Backup loadmasters
		                              \end{itemize}                                                                 & \begin{itemize}
			                                                                                                              \item 1 Pilot
			                                                                                                              \item 1 Co-pilot
			                                                                                                              \item 2 Loadmasters
			                                                                                                              \item 4 Backup loadmasters
		                                                                                                              \end{itemize}                                                               & \checkmark & \ref{sec:cockpit-arrangement}                                                   \\

		Wingspan                    & $\leq$ 80 m when parked                                                                    & 74.65 m                                                                                  & \checkmark & \ref{sec:wing-high-lift-devices}                      \\

		Maximum Payload             & 2,500 nm (Unrefueled)                                                                      & 3088.5 nm (Unrefueled)                                                                   & \checkmark & \ref{sec:maximum-payload}                             \\

		Ferry Range                 & 8,000 nm                                                                                   & 8107 nm                                                                                  & \checkmark & \ref{sec:ferry-mission}                               \\

		Specific Payload            & 5,000 nm at 295,000 lb                                                                     & 5400 nm at 295,000 lb                                                                    & \checkmark & \ref{sec:specific-payload}                            \\

		Service Ceiling             & 43,000 ft                                                                                  & 33,707 ft @ 75$\%$ MTOW                                                                  & \checkmark & \ref{sec:service-ceiling}                             \\

		Cruising Mach $\&$ Altitude & 0.82 Mach $\&$ 31,000 ft                                                                   & 0.75 Mach $\&$ 31,000 ft                                                                 & \checkmark & \ref{sec:mission-profile}, \ref{sec:maximum-payload}  \\

		All weather features        & all weather type and incorporate deicing, terrain following and terminal avoidance systems & Equipped with deicing, terrain following, and terminal avoidance systems                 & \checkmark & \ref{sec:auxiliaries}, \ref{sec:avionics-integration} \\

		Autopilot                   & automated flight control system                                                            & Automated algorithms assist in maintaining stable flight, especially with heavy payloads & \checkmark & \ref{sec:avionics-integration}                        \\ \bottomrule
	\end{tabular}
	\label{tab:RDO}
\end{sidewaystable}

Furthermore, to fulfill the criteria outlined in the Request for Proposal (RFP) for the Heavy Lift Aircraft (HLA), specific design requirements were established. The detailed specifications for the BUING design are presented in Table~\ref{tab:RDO}

\section{Conceptual Design}

After reviewing the features of current heavy lift aircraft, 4 different concepts were generated as part of the ideation process and all 4 were ultimately.

\subsection{Concept 1}


\begin{figure}[H]
	\centering
	\begin{minipage}{.5\textwidth}
		\includegraphics[width=\linewidth]{Structures/Concept_Owl1.pdf}
		\caption{Concept 1}
		\label{fig:owl_1}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\includegraphics[width=\linewidth]{Structures/Concept_Owl2.pdf}
		\caption{Concept 2}
		\label{fig:owl_2}
	\end{minipage}
\end{figure}

The main idea behind the first designs was to produce an aircraft that would give good stability and performance as well as the ability to carry loads in excess of the required minimum.

So the design of concept 1 was quite basic. It is also the base design for the other three modifications that will follow this one. 4 engines, a sharp nose that is very much Boeing 747-esque as well as a conventional tail. At first glance, it gives the impression of similarity to the Ukrainian Antonov AN-124 but with the added benefit of the ability to do mid-air refueling.

This design was scrapped due to the unfavored cross section design of the fuselage which had ‘square’ walls, as well as the disagreement on the configuration on the tail. A visual representation can be seen in Fig.~\ref{fig:owl_1}.

\subsection{Concept 2}

As previously mentioned, the conventional tail idea was shelved due to it being unfavored by the design group. A high T-tail design was drawn up with the same fuselage and nose, but again was shelved due to the disliked design of the fuselage. At first glance, this would seem similar to the Lockheed C5M, with the difference(s) being that one is larger in size and carrying capacity. A visual representation can be seen in Fig.~\ref{fig:owl_2}.
\subsection{Concept 3}

\begin{figure}[H]
	\centering
	\begin{minipage}{.5\textwidth}
		\includegraphics[width=\linewidth]{Structures/Concept_Owl3_5E.pdf}
		\caption{Concept 3-5E}
		\label{fig:owl_3}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\includegraphics[width=\linewidth]{Structures/Concept_OwlX.pdf}
		\caption{Concept X}
		\label{fig:owl_x}
	\end{minipage}
\end{figure}

This one was a more fun concept. Again inspired by the AN225 Mriya, but what if it was not a full six engines. For added power and range, a fifth engine would be placed in between the two horizontal and vertical stabilizers. This would bring back the concept of the ‘trijet’ and is reborn as the 'Quintjet' (Quint- for the 5 engines). The last aircraft to use an odd number of engine configurations include the Lockheed L1011 TriStar, McDonell Douglas DC-10 and MD-11 as well as the Boeing 727, to name a few. A visual representation can be seen in Fig.~\ref{fig:owl_3}.

\subsection{Concept 4}

Concept X --- shown in Fig.~\ref{fig:owl_x} --- takes inspiration from the Blended Wing Body (BWB) UAV that was designed by Boeing and designated as the X-48. It was used as a test bed to investigate the characteristics of BWB aircrafts and was part of a joint research program between Boeing and NASA.  Last flown in 2007, the design seemed radical and different, and so was an interesting option to discover. However, this concept was not researched further due to the possible high costs of operating such an aircraft as well as the struggles for maintenance and stability.
Here, it features 4 engines and no empennage on the fuselage. Unlike the B-2, this concept features empennage(s) that double as wingtips to increase fuel efficiency. The wing will feature high lift devices, ailerons as well as house the elevators. Access is available via a rear door and the interior is entirely 1 floor, apart from the cockpit that is slightly elevated.

\section{Initial Sizing}
\label{cch:initial-sizing}

\begin{table}[H]
	\centering
	\begin{tabular}{l||l||p{2cm}||p{2cm}||>{\raggedright\arraybackslash}p{2cm}||>{\raggedright\arraybackslash}p{1.8cm}}
		\toprule
		Aircraft               & Boeing C-17 & AN-225                  & C5-M          & AN-124                  & Airbus             \\
		                       & Globemaster & Mriya                   & Super Galaxy  & Ruslan                  & 380                \\
		\midrule
		Length (m)             & 53,04       & 84                      & 75.53         & 69.1                    & 73                 \\
		Height (m)             & 16.79       & 18.1                    & 19.84         & 21.08                   & 24.1               \\
		Wingspan (m)           & 51.766      & 88.4                    & 67.91         & 73.3                    & 79.8               \\
		Payload Mass (kg)      & 77,519      & 250,000                 & 127,460       & 150,000                 & 83,000             \\
		MTOW (kg)              & 265,351     & 640,000                 & 381,018       & 402,000                 & 562,000            \\
		Wing Area ($m^2$)      & 353         & 905                     & 576           & 628                     & 843                \\
		Wing Loading (N/$m^2$) & 7,374       & 6937                    & 6489          & 628                     & 6540               \\
		Loading Access         & Aft         & Aft \& Nose             & Nose          & Nose \& Aft             & Aft                \\
		Powerplant             & PW2040      & Ivchenko Progress D-18T & GE CF680C2L1F & Ivchenko Progress D-18T & RR 900 / PW GP7200 \\
		Range (km)             & 4,482       & 4,500                   & 4,445         & 4,500                   & 14,800             \\
		Ferry Range (km)       & 11,540      & 15,400                  & 13,000        & 16,000                  & 17,960             \\
		Takeoff Distance (m)   & 1,064       & 3,500                   & 1,646         & 3,000                   & 2,050              \\
		Landing Distance (m)   & 1,064       & 3,300                   & 1,097         & 2,800                   & 2,900              \\
		Crew                   & 3-5         & 3-22                    & 7             & 8                       & 2                  \\
		\bottomrule
	\end{tabular}
	\caption{Specifications for comparative aircraft analysis.}
	\label{tab:COMPARATIVE AIRCRAFT}
\end{table}

\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.5]{Performance/empty-mtom-hlp.pdf}
	\caption{Benchmark data of empty mass vs maximum take-off mass.}
	\label{fig:EMTOW vs MTOM}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\linewidth]{Performance/mission_profile_BUING.pdf}
	\caption{Mission profile of BUING.}
	\label{fig:missionProfile}
\end{figure}

The matching chart and the gross maximum take-off mass (MTOM) estimation contour are shown in Fig.~\ref{fig:initsizing}. The matching chart consists of performance constraints and stall speed graphs, estimated based on Snorri's constraint analysis for turbofan aircraft. The thrust-to-weight ratio $T/W$ of take-off, cruise, turn, climb, ceiling, and landing performances were calculated for various wing loadings $W/S$. Each line represents $T/W$ required to achieve the parameter values set by the design RFP. The line contour of stall speed $V_{\text{stall}}$ was plotted for various $C_{L_{\text{max}}}$ and $W/S$ and drawn along the matching chart to add more information to the overall design space. The feasible design space that gives better performance is shown in the gray area of the plot.
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.4]{Performance/CONSTRAINT_DIAGRAM.pdf}
	\caption{Matching Chart and Gross MTOM Estimation Contour; grey color represents the feasible design region.}
	\label{fig:initsizing}
\end{figure}

Additionally, a separate calculation was carried out to obtain the gross MTOM (see Eq.~\ref{eq:1}). The calculation was made using Python in-house code (link ...) for various $W/S$ and $T/W$. The $M_{\text{empty}}$ is defined using the empirical model constructed from the benchmark aircraft data (see Fig~\ref{fig:EMTOW vs MTOM}). The payload mass $M_{\text{payload}}$ is $\sim 195,000$ kg. The fuel mass $ M_{\text{fuel}}$ is calculated based on the defined mission profile (see Fig.~\ref{fig:missionProfile}). The fuel mass fraction for each mission segment was calculated based on Method 3 of Initial Gross Estimation in Snorri's Chapter 6. The lift-to-drag ratios used for cruising flights with a distance of $4630$ km (2500 nm) and loitering segments are estimated for various wing areas using the panel method based on Prandt'l Lifting Line Theory, where the $C_{D_{\text{min}}}$ was assumed to be constant, which is $\sim 0.025$.

Prandtl's compressibility correction was also taken into account for cruise and loitering segments. Here, it is worth noting that other wing geometric parameters, except wing area, are assumed constant by referring to other aircraft competitors (see InputReq.py for details of the assumed parameters). By assuming the initial MTOM, the iteration will run until the final MTOM is obtained based on Eq.~\ref{eq:1}  was obtain for various wing areas and $T/W$ that are able to meet the design requirement. The gross MTOM results obtained from the calculation are then coincidentally plotted with the matching chart to decide the initial design point for our aircraft. For the initial sizing, the current design is assumed using MTOM$\approx 480,000$ kg, $T/W = 0.25$, and $W/S = 6480$ N/m$^{2}$ shown by a black point in the Fig.~\ref{fig:initsizing}.
\begin{equation}
	\label{eq:1}
	MTOM = M_{\text{empty}}+ M_{\text{payload}} + M_{\text{fuel}}
\end{equation}
