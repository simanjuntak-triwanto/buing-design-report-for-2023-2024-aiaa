\chapter{Propulsion} % Main chapter title, Chapter 5
\label{ch:propulsion} % for referencing, use\ref{ChapterX}

%----------------------------------------------------------------------------------------
\section{Engine Selection}
\label{sec:engine-selection}
%----------------------------------------------------------------------------------------


The propulsion system for BUING is carefully crafted to install four turbofan engines Rolls-Royce Trent-1000, as shown in Fig.~\ref{fig:engine}. We've placed significant emphasis on performance, efficiency, and reliability, making the Engines Rolls-Royce Trent series the perfect fit for our needs. These engines boast impressive thrust capabilities, low specific fuel consumption, and a minimized environmental footprint, aligning perfectly with our project goals. This link \url{https://tinyurl.com/2yycsnqt} contains engine data set and relevant information that were used in selecting the engine.

In selecting the Engines Rolls-Royce Trent-1000 engines for BUING, several factors were taken into account. Firstly, the thrust requirement for our aircraft stands at approximately $\sim 1200$ $\mathrm{kN}$, estimating from the initial sizing analysis in Section~\ref{cch:initial-sizing} for $T/W=0.25$. Each Rolls-Royce Trent-1000 engine delivers a thrust of 360.4 $\mathrm{kN}$, summing up to a total thrust of 1441.6 kN, as detailed in Table~\ref{tab:trent-1000-specs}. This not only meets but exceeds our required thrust, providing us with ample power to achieve optimal performance.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.5\linewidth]{Propulsion/trent-1000.png}
	\caption{Rolls-Royce Trent-1000 \protect\cite{Trent1000}.}
	\label{fig:engine}
\end{figure}

\begin{table}[ht!]
	\centering
	\caption{Rolls-Royce Trent-1000 specifications \protect\cite{RollsRoyceTrent10002024, RollsRoyceTrent2024,}.}
	\begin{tabular}{l||r}
		\toprule
		Thrust (kN)            & 360.4            \\
		Continuous Thrust (kN) & 324.36           \\
		Total Thrust (kN)      & 1441.6           \\
		Length (m)             & 4.738            \\
		Fan Diameter (cm)      & 285              \\
		Dry Weight (kg)        & $5,936-6,120$    \\
		TSFC 1/h (cruising)    & 0.505            \\
		Bypass Ratio           & $\approx 10.1$   \\
		Overall Pressure Ratio & $50: 1$          \\
		Thrust/Weight Ratio    & 6.01             \\
		First Run              & 14 February 2006 \\
		\bottomrule
	\end{tabular}
	\label{tab:trent-1000-specs}
\end{table}

%----------------------------------------------------------------------------------------
\subsection{Engine Candidates}
%----------------------------------------------------------------------------------------

In order to choose the best turbofan engine for BUING we had to choose a several engines which they have a thrust bigger than 100 $\mathrm{kN}$.  After we choose the engines that have thrust bigger than 100 $\mathrm{kN}$, we start choosing the engines that have a thrust Bigger than 200 $\mathrm{kN}$, and start doing performance and TSFC calculations to choose or have the best candidates for our BUING to see our spreadsheet for necessary computations.

Furthermore, we meticulously evaluated the Total Specific Fuel Consumption ($\mathrm{TSFC}$) data, converting $\frac{\mathrm{g}}{\mathrm{kN}{\mathrm{s}}}$ to $\frac{1}{\mathrm{h}}$, to ensure that our engine selection aligns with our objectives of maximizing performance while minimizing fuel consumption. By choosing the Rolls-Royce Trent-1000 engines, we can achieve the right balance between power output and fuel efficiency.

In summary, the Rolls-Royce Trent-1000 engines emerged as the most suitable choice for BUING due to their exceptional thrust capabilities, low specific fuel consumption, and compatibility with our performance and efficiency goals.

\section{Engine Characteristics}
%----------------------------------------------------------------------------------------

The Trent 1000 engine represents a pinnacle in aviation engineering, merging innovation with reliability for modern air travel demands \cite{RollsRoyceTrent10002024}. It comprises three coaxial shafts:
\begin{itemize}
	\item The low-pressure shaft, boasting a 2.85-meter fan powered by six axial turbines, ensures robust propulsion.
	\item The intermediate pressure spool, with eight axial compressors and a single turbine stage, facilitates seamless operation.
	\item The high-pressure compressor, driven by a solitary turbine stage, prioritizes efficiency .
\end{itemize}

An Electronic Engine Controller (EEC) ensures precise performance. Originally, Boeing considered exclusively partnering with GE Aviation for the 787's engine. However, responding to market demands, Boeing embraced diversity, allowing integration with both GE and Rolls-Royce engines, offering airlines unprecedented flexibility with necessary modifications.

The Trent 1000 program is a collaboration among six partners, sharing risks and rewards for excellence and innovation: Kawasaki Heavy Industries, Mitsubishi Heavy Industries, Industria de Turbo Propulsores, Carlton Forge Works, Hamilton Sundstrand, and Goodrich Corporation.

Inspired by predecessors like the Trent 8104, the Trent 1000 adopts a "more-electric" engine paradigm, featuring a bleed-less design and a meticulously crafted fan for optimal airflow efficiency and increased bypass ratio. Efficiency is further improved with a high-pressure ratio and contra-rotating spools, simplifying maintenance and reducing costs. A tiled combustor highlights its commitment to emissions reduction and environmental sustainability.

%----------------------------------------------------------------------------------------
\section{Engine Performance}
%----------------------------------------------------------------------------------------

In our engine performance calculations, we considered Mach number, various altitude, throttle ratio, temperature ratio, and pressure ratio, in order to have the best and accurate engine performance, with low TSFC, as shown in Fig.~\ref{fig:thrust_ratio}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.65\linewidth]{Propulsion/thrusts_vs_mach_vs_altitude_turbofan_buing.pdf}
	\caption{Thrust ratio vs Mach number with various altitudes.}
	\label{fig:thrust_ratio}
\end{figure}

The references \cite{gudmundssonGeneralAviationAircraft2022, mattinglyAircraftEngineDesign2004} provides methods for estimating the impact of altitude and airspeed on turbofan engine thrust. The results in Fig.~\ref{fig:thrust_ratio} were used when computing the performance in Chapter~\ref{ch:performance}.

The thrust ratio is crucial for assessing the efficiency and performance of an aircraft's propulsion system, especially at higher altitudes. As altitude increases, the thrust ratio's importance grows, affecting variables like fuel consumption, engine efficiency, and overall flight dynamics. Understanding these relationships is essential for optimizing aircraft performance and fuel efficiency across varying flight conditions.

%----------------------------------------------------------------------------------------
