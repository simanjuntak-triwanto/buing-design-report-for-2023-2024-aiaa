\chapter{Stability} % Main chapter title
\label{ch:stability} % for referencing this chapter, use \ref{ChapterX}

%---------------------------------------------------------------------
\section{Empennage Design}
% ---------------------------------------------------------------------

The design of the empennage plays a significant importance role in the trim,stability and control of the aircraft. The empennage contributes significantly to longitudinal stability by counteracting pitching moments generated during flight and facilitating lateral and directional stability, crucial for maintaining straight and level flight. Given the typically larger wings and higher lift coefficients inherent in BUING designs, the empennage must effectively counteract the increased pitching moments and yaw in tendencies encountered during various flight phases, from take-off and landing to high-lift configurations.

The chosen tail configuration is an H-tail empennage configurations shown in Fig.~\ref{fig:htail_design} and it necessitates careful consideration of performance objectives, aerodynamic efficiency, and operational requirements. The H-tail configuration is also used by the largest cargo aircraft in the world, which is the AN-225 Mriya \cite{AntonovAn225Mriya2024}. The H-tail empennage design stands as a cornerstone of stability engineering in aircraft design, offering a nuanced balance of longitudinal, lateral, and directional stability crucial for safe and predictable flight. By virtue of its configuration, the H-tail empennage enhances longitudinal stability through careful placement of horizontal stabilizers, strategically positioned behind the center of gravity. The H-tail design typically offers a larger usable cargo area between the horizontal stabilizers compared to other tail configurations \cite{gudmundssonGeneralAviationAircraft2022}. This allows for greater flexibility in arranging and securing payloads of various sizes and shapes. In this case, the spacious cargo area between the horizontal stabilizers can accommodate the large dimensions of the M1 Abrams tanks and the numerous pallets, enabling efficient loading and securing of the cargo without compromising structural integrity. Moreover, the H-tail empennage design bolsters lateral stability by countering adverse roll tendencies. Through symmetric placement of horizontal stabilizers relative to the aircraft's longitudinal axis, the H-tail configuration effectively mitigates roll disturbances induced by asymmetric aerodynamic forces, such as those experienced during crosswinds or maneuvering. This inherent lateral stability fosters a wings-level orientation, reducing the need for continuous corrective inputs and promoting smoother, more stable flight trajectories.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.5\linewidth]{Stability/H-Tail-Crop.pdf}
	\caption{H-Tail design.}
	\label{fig:htail_design}
\end{figure}

%---------------------------------------------------------------------
\subsection{Stabilizer Sizing}
%---------------------------------------------------------------------

To properly size the horizontal and vertical stabilisers, a thorough study of critical geometric characteristics was required, particularly volume ratios and moment lever arms between the tail aerodynamic centre and the aircraft's centre of gravity. Despite the availability of historical data on these factors, much of it had become obsolete. As a result, trade studies were conducted to determine the volume ratios and moment lever arms for both the horizontal and vertical stabilisers. Comparative tail sizing was performed to estimate tail volume ratios, and the moments arms of similar aircraft to BUING are shown in Table \ref{fig:comparison_aircraft}.

\begin{table}
	\centering
	\caption{Comparison stabilizer with similar.}
	\begin{tabular}{c||r||r||r}
		\toprule
		Parameter & C-5 M Super Galaxy  & AN-225 Mriya        & BUING               \\ \midrule
		CHT       & 0.20                & 0.15                & 0.5                \\
		CVT       & 0.05                & 0.10                & 0.08                \\
		LHT       & $12.19 \mathrm{~m}$ & $13.72 \mathrm{~m}$ & $12.95 \mathrm{~m}$ \\
		LVT       & $9.14 \mathrm{~m}$  & $10.67 \mathrm{~m}$ & $9.88 \mathrm{~m}$  \\
		\bottomrule
	\end{tabular}
	\label{fig:comparison_aircraft}
\end{table}

By using the Langley symmetrical supercritical airfoil with max thickness 11$\%$ at 40$\%$ chord it offers the BUING design a great advantage in achieving optimal aerodynamic, performance, especially in configurations where high lift coefficients and efficient low-speed operation are paramount. The Langley SC airfoil is optimized for high Reynolds number performance and is known for its delayed shock wave formation and reduced wave drag at transonic speeds. The geometry of the airfoil is shown in Fig.~\ref{fig:langley_airfoil}.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{Stability/Langley-Symmetrical-SC.pdf}
	\caption{Langley symmetrical supercritical airfoil.}
	\label{fig:langley_airfoil}
\end{figure}

The horizontal tail design parameter was constructed through a comparison study of sizing from other military cargo aircraft shown in Table \ref{fig:comparison_aircraft}. By angling the horizontal tail up, the aircraft improves its inherent stability against rolling motions. This is especially critical for heavy-lift aircraft, which frequently transport huge, heavy payloads. It was chosen to include a horizontal tail dihedral angle $\Gamma = \SI{3}{\degree}$, which offers a fair mix of stability and maneuverability. It reduces the destabilizing effects of side gusts and turbulence encountered during flight, allowing the aircraft to maintain a more regular and predictable attitude.

\begin{table}
	\caption{Horizontal stabilizer parameters.}
	\centering
	\begin{tabular}{l||r}
		\toprule
		Parameter & Value                  \\ \midrule
		$b$       & $27 \mathrm{~m}$       \\
		$S_{H T}$ & $162 \mathrm{~m}^2$    \\
		$\lambda$ & 0.6                    \\
		$A R$     & 4.50                   \\
		$V_{H T}$ & $453.33 \mathrm{~m}^3$ \\
		$M_{G C}$ & \SI{6}{\meter}         \\ \bottomrule
	\end{tabular}
	\label{fig:horizonta_parameter}
\end{table}


\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.6\linewidth]{Stability/Horizontal_Stab_Dims.pdf}
	\caption{Geometry of the horizontal stabilizer.}
	\label{fig:horizontal_stabilizer}
\end{figure}

%---------------------------------------------------------------------
\subsection{Vertical Stabilizer}
%---------------------------------------------------------------------

A preliminary design phase estimate of SVT was selected. This was repeated until the stability derivatives fell within Raymer’s suggested ranges. This estimate was put to the test while figuring out where to place the engines on the wing so that the rudder could counteract yaw caused by an inoperable engine. A vertical tail area of \SI{38}{\square\meter} was the consequence. The BUING vertical tail, which is essential for yaw stability and control, is carefully crafted to guarantee peak performance within the aircraft's operating limitations. The vertical tail, also known as the vertical stabiliser, prevents the aircraft from yawing or swinging side to side by providing stability in the yaw axis. Its dimensions and form are precisely that is shown in Fig.~\ref{fig:vertical_stabilizer} to produce enough aerodynamic forces to offset yawing moments brought on by things like aircraft sideslip, crosswinds, and asymmetric engine thrust. The summary of the vertical tail parameter is shown in Table \ref{fig:vertical_parameter}.

\begin{table}
	\caption{Vertical stabilizer parameters.}
	\centering
	\begin{tabular}{l||l}
		\toprule
		Parameter & Value                  \\ \midrule
		$b$       & \SI{8}{\meter}         \\
		$S_{H T}$ & \SI{38}{\square\meter} \\
		$\lambda$ & 0.58                   \\
		$A R$     & 1.68                   \\
		$V_{V T}$ & $9.13 \mathrm{~m}^2$   \\
		$M_{G C}$ & \SI{4.75}{\meter}      \\ \bottomrule
	\end{tabular}
	\label{fig:vertical_parameter}
\end{table}



\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.5\linewidth]{Stability/Vertical_Stab_Dims.pdf} %Vertical-Tail-Crop.pdf}
	\caption{Geometry of the vertical stabilizer.}
	\label{fig:vertical_stabilizer}
\end{figure}

% ---------------------------------------------------------------------
\section{Control Surface Sizing}
%---------------------------------------------------------------------

The BUING used a variety of control surfaces, including elevators, rudders, and ailerons, to maintain aircraft control. All of these controls were sized using Raymer's conceptual design technique \cite{raymerAircraftDesignConceptual2018}. All the sizing calculations are designed for a maximum deflection of 25 degrees for elevators, rudders, and ailerons. While maintaining the restriction of a maximum 25 degrees deflection for control surfaces, these proportions provide for steady flight and control authority. By having balance surface area and efficacy to obtain accurate handling characteristics, which are especially important while navigating difficult terrain or transporting large cargoes. Furthermore, moment lever arms are carefully chosen to retain aerodynamic efficiency while providing adequate control authority for both horizontal and vertical stabilizers. The scaled dimensional drawings of the control surfaces are
shown in \ref{fig:vertical_stabilizer} and \ref{fig:horizontal_stabilizer}

\section{Trim Analysis}

The trim condition of an aircraft is an important part of longitudinal static stability, since it ensures that the aircraft can maintain a desired equilibrium state without continuous control inputs from the pilot. Longitudinal trim assessments were carried out at take-off, cruise, and landing conditions by solving the trim equations. The lift increment and pitching moment contribution from the flaps were considered during takeoff and landing. The pitching moment around the aerodynamic centre was modified for sweep, taper, and twist. Trim analysis charts were created for take-off, cruise, and landing, and are shown in Fig.~\ref{fig:trim_diagram}. The goal is to achieve zero pitching moment about the centre of gravity during cruise without any tail angle of incidence.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.75\linewidth]{Stability/TRIM-PLOT.pdf}
	\caption{Trim diagram}
	\label{fig:trim_diagram}
\end{figure}

%---------------------------------------------------------------------
\section{Stability Derivatives}
%---------------------------------------------------------------------

%---------------------------------------------------------------------
\subsection{Longitudinal Stability Derivatives}
%---------------------------------------------------------------------

The Longitudinal Stability and Control Derivatives play a critical role in defining the dynamic response and handling characteristics of the BUING design. These derivatives encapsulate the aircraft's inherent stability, controllability, and maneuverability in longitudinal motion, encompassing parameters such as pitch stability, elevator effectiveness, and dynamic response to control inputs. The Longitudinal Stability and Control Derivatives are presented at cruising condition in the Table~\ref{tab:longitudinal_derivatives}.

\begin{table}
	\caption{Longitudinal stability derivatives.}
	\centering
	\begin{tabular}{c||r}
		\toprule
		Derivative       & Value   \\ \midrule
		$C_{M_{\alpha}}$ & -4.5832 \\
		$C_{M_{q}}$      & -27.864 \\
		$C_{L_{\alpha}}$ & 4.9788  \\
		$C_{L_{q}}$      & 15.39   \\ \bottomrule
	\end{tabular}
	\label{tab:longitudinal_derivatives}
\end{table}

Since $C_{M_{\alpha}}$ is negative, the aircraft is longitudinally statically stable. The pitch stiffness (ratio of $C_{M_{\alpha}}$ to $C_{L_{\alpha}}$) determines how stable the aircraft is, i.e. its static margin. Furthermore, with a forward CG of 37.91 metres and an aft CG of 26.88 metres, the aircraft has a significant static margin of 92.06\%, shown in Table \ref{tab:static_margin}, ensuring a balanced and predictable response to flight inputs. Furthermore, the neutral point, located at 15.13 metres, demonstrates the great attention paid to aerodynamic balance. As the magnitude of pitch stiffness increases, the static margin increases. $C_{M_{q}}$ determines how changes in pitch rate affect the pitching moment of the aircraft. From Table \ref{tab:longitudinal_derivatives}, it shows that negative $C_{M_{q}}$ is essential to meet the short period damping requirement.

\begin{table}
	\caption{Static margin and Neutral Point.}
	\centering
	\begin{tabular}{c||r||c||c||c}
		\toprule
		Parameter             & Value                                                   \\ \midrule
		Neutral Point         & Static Margin & Forward CG         & Aft CG             \\ \midrule
		\SI{15.13363}{\meter} & {92.06}{\%}   & \SI{37.91}{\meter} & \SI{26.88}{\meter} \\ \bottomrule
	\end{tabular}
	\label{tab:static_margin}
\end{table}

%---------------------------------------------------------------------
\subsection{Lateral-Directional Stability Derivatives}
%---------------------------------------------------------------------

The Lateral-directional stability and control derivatives are fundamental parameters that define the dynamic response and handling characteristics of the BUING design in roll and yaw motions. These derivatives encapsulate the aircraft's inherent stability, controllability, and maneuverability in lateral and directional motion, including parameters such as roll damping, yaw stability, and control surface effectiveness. These derivatives are presented at cruising condition in the Table \ref{tab:lateral_derivatives}.

\begin{table}
	\caption{Lateral-directional stability derivatives.}
	\centering
	\begin{tabular}{c||r}
		\toprule
		Derivative      & Value     \\ \midrule
		$C_{l_{\beta}}$ & -0.071015 \\
		$C l_p$         & -0.43424  \\
		$C l_r$         & 0.082939  \\
		$C n_B$         & 0.098659  \\
		$C n_p$         & -0.011236 \\
		$C n_r$         & -0.11697  \\ \bottomrule
	\end{tabular}
	\label{tab:lateral_derivatives}
\end{table}

A negative $C_{l_{\beta}}$ implies static lateral stability. $C_{l_{p}}$ is the roll damping derivative. It is necessary for $C_{l_{p}}$ to be negative to meet roll handling requirements. A positive $C_{n_{\beta}}$ implies static directional stability. Since $C_{n_{r}}$ is the yaw damping derivative, it must be negative to meet yaw to meet handling requirements from \cite{MILF8785BASGMilitary}.

%---------------------------------------------------------------------
\section{Handling Qualities Analysis}
%---------------------------------------------------------------------

Handling qualities represent the integrated value of those and other factors and are defined as “those qualities or characteristics of an aircraft that govern the ease and precision with which a pilot is able to perform the tasks required in support of an aircraft role and it encompass a multifaceted assessment of its dynamic response, controllability, and pilot interface, ultimately shaping the aircraft's ease of operation, maneuverability, and overall flight performance. The handling qualities are evaluated based on MIL-F4785B \cite{MILF8785BASGMilitary} standards for large heavy transport aircraft. Based on this, Level1 flying qualities are desired for the BUING.

%---------------------------------------------------------------------
\subsection{Longitudinal dynamic Handling Qualities Analysis}
%---------------------------------------------------------------------

The longitudinal dynamic flying qualities is to maintain a stable and predictable longitudinal motion during various flight conditions which often operate in demanding low-speed regimes and utilize high-lift devices for enhanced lift during take-off and landing, achieving adequate longitudinal dynamic stability is paramount for safe and efficient flight operations. This dynamic stability is crucial for maintaining a steady pitch attitude, especially during critical flight phases such as climb, descent, and transitions between high-lift and cruise configurations. The dynamic characteristic has been analyzed in XFLR5 \cite{Xflr5}, from which dynamic modes were determined as well as other characteristics. The Table \ref{fig:longitudinal_dynamic} shows the Level 1 requirements for longitudinal flying qualities for MTOM at trim condition. The real number shows negative value, impliying that the disturbance is dynamically damped. This behaviour for short oscillation and phugoid modes are shown in Fig.~\ref{fig:time-longitudinal}. Here the analysis was made for the dynamic behaiour of  pitch angle $\theta$ at three conditions, that is trim, take-off ($\delta_{s}=15$ deg and $\delta_{s}=30$), landing ($\delta_{s}=15$ deg and $\delta_{s}=40$). Note that the stable behaviour for landing and take-off configurations was achieved when the elevator was deflected at -20 and -25 degree, respectively.

\begin{table}[H]
	\centering
	\caption{Longitudinal dynamic stability analysis.}
	\begin{tabular}{l||l||c||c} \toprule
		Dynamic Mode & Roots                         & $\omega_n$ (\unit{Hz}) & $\boldsymbol{\zeta}$ \\  \midrule
		Phugoid      & $-0.00042+0.05464 \mathrm{i}$ & $0.009$                & 0.008                \\
		Short        & $-1.601+-3.761 \mathrm{i}$    & $0.6506$               & 0.391                \\ \bottomrule
	\end{tabular}
	\label{fig:longitudinal_dynamic}
\end{table}

\begin{figure}[htb!]
	\begin{minipage}[t]{.5\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{Stability/longitudinal-short.pdf}
	\end{minipage}
	%\hfill
	\begin{minipage}[t]{.5\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{Stability/phugoid-time.pdf}
	\end{minipage}
	\caption{Time-variant $\theta$ for longitudinal modes.  Left figure is short oscillation mode and right figures is phugoid mode.}
	\label{fig:time-longitudinal}
\end{figure}

%---------------------------------------------------------------------
\subsection{Lateral-Directional Dynamic Stability Analysis}
%---------------------------------------------------------------------

The lateral-directional dynamic stability is a pivotal aspect of flying qualities for BUING as a Heavy-Lift Aircraft (HLA), ensuring the aircraft's ability to maintain stable and predictable motion in roll and yaw axes during various flight conditions. It encompasses the aircraft's response to disturbances in roll and yaw, including its natural tendency to return to its trimmed condition following perturbations. A well-designed HLA should exhibit desirable dynamic stability traits in roll and yaw, including prompt response to pilot inputs, coordinated turns, and effective damping of oscillatory motions. Table \ref{tab:lateral_dynamic} shows lateral-directional dynamic stability, has met level 1 requirements for lateral-directional flying qualities.  Like the longitudinal mode, all the real number values are negative for MTOM at trim condition for roll, spiral, and dutch roll modes.  Fig.~\ref{fig:time-lateral} show the dynamic behaviour of roll angle $\phi$ for those three modes, compared with  landing and take-off configurations when elevator are deployed at -20 and -25 degree. Only the spiral mode for take-off and landing configuration are unstable. Nevertheless, since it is a long-period mode, the condition is less catastrophic. The augmented control system can be used during those manuever.


\begin{table}[H]
	\centering
	\caption{Lateral-directional dynamic stability.}
	\begin{tabular}{l||l||l||l||l||l} \toprule
		Dynamic Mode & Roots                           & $\omega_{\boldsymbol{n}}$ & $\zeta$ & $T_{2}~(\unit{s})$ & $ \tau $ \\ \midrule
		Roll         & $-4.874+0.00000 \mathrm{i}$     & -                         & -       & $0.142$            & 0.205    \\
		Spiral       & $-0.0001145+0.00000 \mathrm{i}$ & -                         & -       & $6054.25$          & 8734.43  \\
		Dutch Roll   & $-0.1477+-1.373 \mathrm{i}$     & $0.219 \unit{Hz}$         & 0.107   & -                  & -        \\ \bottomrule
	\end{tabular}
	\label{tab:lateral_dynamic}
\end{table}

\begin{figure}[htb!]
	\begin{minipage}[t]{.5\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{Stability/lateral-roll.pdf}
	\end{minipage}
	%\hfill
	\begin{minipage}[t]{.5\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{Stability/lateral-dutchmode.pdf}
	\end{minipage}

	\begin{minipage}[t]{1\textwidth}
		\centering
		\includegraphics[width=.5\textwidth]{Stability/lateral-spiralmode.pdf}
	\end{minipage}
	\caption{Time-variant $\phi$ for lateral-directional modes.  Upper left figure is roll mode, upper right figures is dutch mode, and lower figure reprents spiral mode.}
	\label{fig:time-lateral}
\end{figure}

%---------------------------------------------------------------------
\section{Weight Estimation}
%---------------------------------------------------------------------

The concept “known weights” refers to parts and component that can either be weighed with reasonable accuracy or whose manufacturer (if the component is obtained from an outside vendor) can disclose the weight with reasonable confidence. Most of the time the weight analyst uses all three methods simultaneously, but known weights always supersede both the statistical and direct weight estimations. Engines, propellers, wheels, tires, brakes, landing gear struts, and standard parts(electronics, avionics, antennas, instruments, fasteners, etc.) are examples of components that will likely have published weights. The estimation weight techniques that are being used are Cessna's, Raymer's and Torenbeek's equations. From this three methods, Raymer's and USAF's have overestimated weight estimation, unlike Torenbeek's has the nearest weight estimation value. The breakdown weight estimations for all major fixed components are shown in Table \ref{tab:weight_list}. This data were used as the input to the XFLR5 model for the aerodynamics and stability analysis.

\begin{table}[H]
	\centering
	\caption{Weight components.}
	\begin{tabular}{c||>{\raggedright} p{3cm}||r||r||r}
		\toprule
		No.                    & Component                                       & Raymer (lb) & Torrenbeek (lb) & USAF (Ib)   \\ \midrule
		1                      & Wing                                            & 127,318     & 106,042         & 165,481     \\
		2                      & Horizontal Tail                                 & 9,081       & 0               & 240,633     \\
		3                      & Vertical Tail                                   & 12,613      & 0               & 2,490       \\
		4                      & Emmpenage                                       & 21,695      & 23,796          & 60,260      \\
		5                      & Fuselage                                        & 42,866      & 0               & 197,116     \\
		6                      & Main Landing Gear                               & 20,205      & 46,189          & 3,481       \\
		7                      & Nose Landing Gear                               & 1,900       & 6,065           & 0           \\
		8                      & Nacelle                                         & 0           & 14,124          & 0           \\
		9                      & Engine Dry                                      & 79,559      & 79,559          & 79,559      \\
		10                     & Installed Engine                                & 339,841     & 368,194         & 755,546     \\
		11                     & Fuel System                                     & 8,020       & 1,429           & 1,974       \\
		12                     & Flight Control System                           & 160,960     & 6,769           & 18,067      \\
		13                     & Hydraulic System                                & 6,974       & 1,080           & 1,080       \\
		14                     & Avionics Systems                                & 3,086       & 3,086           & 3,086       \\
		15                     & Electrical System                               & 1,454       & 134,538         & 973         \\
		16                     & Air Conditioning, Pressurization, and Antiicing & 32,770      & 32,770          & 32,770      \\
		\multirow[t]{2}{*}{17} & Furnishing                                      & 62,806      & 0               & 227         \\ \midrule
		                       & Total Empty Weight                              & 931,148     & 823,641         & $1,608,751$ \\ \toprule
	\end{tabular}
	\label{tab:weight_list}
\end{table}
