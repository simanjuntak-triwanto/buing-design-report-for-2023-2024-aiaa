\chapter{Cost Analysis} % Main chapter title
\label{ch:cost} % for referencing this chapter, use \ref{ChapterX}

In this chapter, we provide a concise summary of LCC costs and DOC. For a more comprehensive analysis and detailed calculations, please refer to the following link: \url{https://tinyurl.com/26wvyuxk}.

%----------------------------------------------------------------------------------------
\section{Life Cycle Cost (LCC)}
%----------------------------------------------------------------------------------------

Table~\ref{tab:dte} provides a summary of the development, test, and evaluation costs of BUING. This summary was derived using Nicolai's method \cite{nicolaiFundamentalsAircraftAirship2010}, where the costing timeframe is set to 1998, resulting in all costs being converted to USD values of that year.

In this report, since avionics data price was not publicly available, the cost was computed using Raymer's method \cite{raymerAircraftDesignConceptual2018}, which estimates the cost of avionics per unit mass in the year 2012. Additionally, this LCC calculation assumes the unit cost of the engine as given in \cite{scottJapaneseAirlineANA2016}. An average annual inflation rate of 2\% was assumed, with a total of 6 aircraft for flight tests and a production quantity of 90 aircraft for costing purposes

\begin{table}[H]
	\centering
	\caption{The development, test, and evaluation cost.}
	\begin{tabular}{lr}
		\begin{tabular}{l||r}
			\toprule
			Time frame for costing (Year)          & 1998            \\ \midrule
			Annual Inflation Rate                  & $0.02$          \\
			Flight test aircraft number, QD (unit) & 6               \\
			Production quantity for costing (unit) & 90              \\
			Airframe engineering (USD)             & $3,553,538,672$ \\
			Development support (USD)              & $730,705,244$   \\
			Engines (USD)                          & $420,095,625$   \\
			Avionics (USD)                         & $336,787,535$   \\
			Manufacturing labor (USD)              & $1,504,460,505$ \\
			Material and equipment (USD)           & $510,898,328$   \\
			Tooling (USD)                          & $1,639,201,363$ \\
			Quality control (USD)                  & $214,860,195$   \\
			Flight test operations (USD)           & 177,178,899     \\
			Test facilities (USD)                  & 0               \\
			Total DT\&E Cost                       & $9,087,726,366$ \\
			Flight test aircraft (USD)             & $4,626,303,551$ \\
			\bottomrule
		\end{tabular}
	\end{tabular}
	\label{tab:dte}
\end{table}


The flyaway cost is outlined in Table~\ref{tab:flyaway}. These tables provide a breakdown of the flyaway cost for projected productions of 90, 180, and 270 aircraft. Amortization for each production number is set at 100\%, indicating that the total cost for DT\&E would be distributed across the entire projected production. To achieve a 10\% profit margin, the unit price of the aircraft in 2022 may range from 480 million USD to 700 million USD, contingent upon the production quantity.

\begin{table}[H]
	\centering
	\caption{Flyaway cost for nominal production of 90, 180, and 270 aircraft.}
	\begin{tabular}{p{6cm}||r||r||r}
		\toprule
		Number of nominal production                      & $90$             & $180$            & $270$            \\ \midrule
		Engines                                           & $5,041,147,500$  & $10,082,295,000$ & $15,123,442,499$ \\
		Avionics                                          & $5,051,813,024$  & $10,103,626,048$ & $15,155,439,072$ \\
		Manufacturing labor (USD)                         & $7,392,091,057$  & $12,089,425,917$ & $16,002,411,694$ \\
		Material and equipment (USD)                      & $4,446,634,130$  & $7,736,675,180$  & $10,696,736,056$ \\
		Sustaining engineering (USD)                      & $2,030,296,671$  & $2,665,925,956$  & $3,079,164,661$  \\
		Tooling (USD)                                     & $1,759,522,353$  & $2,405,257,589$  & $2,847,607,988$  \\
		Quality control (USD)                             & $1,055,704,765$  & $1,726,556,728$  & $2,285,391,528$  \\
		Manufacturing facilities (USD)                    & 0                & 0                & 0                \\
		Subtotal for all aircraft for costing             & $26,777,209,500$ & $46,809,762,417$ & $65,190,193,498$ \\
		Profit margin rate                                &                  &                  & 0.1              \\
		Unit cost aircraft (USD)                          & $297,524,550$    & $260,054,236$    & $241,445,161$    \\
		Percentage of produced aircraft for ammortization & $100$            & $100$            & $100$            \\
		Number of aircraft for amortization               & $90$             & $180$            & $270$            \\
		Additional price per aircraft due ammortization (DTE Cost is given to N aircraft) (USD)
		                                                  & $100,974,737$    & $50,487,369$     & $33,658,246$     \\
		Final unit cost + Ammortization (USD)             & $398,499,287$    & $310,541,604$    & $275,103,407$    \\
		Unit Cost + profit margin                         & $438,349,216$    & $341,595,765$    & $302,613,748$    \\ \midrule
		Ajusted price to Y2022 (USD)                      & $705,057,208$    & $549,435,352$    & $486,735,224$    \\
		\bottomrule
	\end{tabular}
	\label{tab:flyaway}
\end{table}



%----------------------------------------------------------------------------------------
\section{Direct operating cost}
%----------------------------------------------------------------------------------------

To gauge the direct operating cost of BUING, we employed the method outlined by Roskam \cite{roskamAirplaneDesign2002}. Primary parameters' assumptions are detailed in Table~\ref{tab:parameters-for-doc}, while the calculation summary is presented in Table~\ref{tab:summary-for-doc}. Roskam's method operates within a costing timeframe set to Year 1990, necessitating adjustments of all component costs to that year. The final DOC per flight hour was determined under the premise of operating a fleet comprising 90 aircraft.

\begin{table}[H]
	\centering
	\caption{Parameters to calculate program cost (direct operating cost).}
	\begin{tabular}{l||l||r}
		\toprule
		No & Parameter                            & Value \\ \midrule
		1  & Time frame for costing (year)        & 1990  \\
		2  & Factor of oil and lubricants         & 1.005 \\
		3  & Flight hour/year                     & 780   \\
		4  & Block hour                           & 11.35 \\
		5  & FP (USD/gallon)                      & 0.75  \\
		6  & FD (lbs/gallon)                      & 6.55  \\
		7  & Number of crew                       & 4     \\
		8  & Crew ratio                           & 1.5   \\
		9  & Crew overhead factor                 & 3     \\
		10 & Maintenance manhours per flight hour & 24    \\
		11 & Indirect personnel cost factor       & 0.2   \\
		12 & Spares cost factor                   & 0.14  \\
		13 & Depot cost factor                    & 0.15  \\
		14 & Annual inflation rate                & 0.02  \\
		\bottomrule
	\end{tabular}
	\label{tab:parameters-for-doc}
\end{table}

As indicated in Table~\ref{tab:summary-for-doc}, the direct operating cost per flight hour amounts to approximately 66,000 USD in the year 2022. Detailed computations for deriving the DOC can be found in the spreadsheet linked above.

\begin{table}[H]
	\centering
	\caption{Summary of the program cost (direct operating cost).}
	\begin{tabular}{c||l||r||r}
		\bottomrule
		No & Cost Component                                       & Y1990 USD        & Y2022 USD        \\ \midrule
		1  & Fuel, oil, and lubricants                            & $19,719,784,064$ & $37,162,733,537$ \\
		2  & Direct personal, aircrews, and maintenance personnel & $2,842,184,937$  & $5,356,212,884$  \\
		3  & Consumable materials                                 & $207,559,666$    & $391,154,617$    \\
		4  & Miscellany                                           & $830,238,666$    & $1,564,618,466$  \\
		5  & Indirect personnel                                   & $9,254,810,719$  & $17,441,066,472$ \\
		6  & Spares                                               & $6,478,367,503$  & $12,208,746,531$ \\
		7  & Depot                                                & $6,941,108,039$  & $13,080,799,854$ \\
		\midrule
		   & Total DOC                                            & $46,274,053,595$ & $87,205,332,361$ \\
		\midrule
		   & DOC Per Flight Hour                                  & $35,009$         & $65,977$         \\
		\bottomrule
	\end{tabular}
	\label{tab:summary-for-doc}
\end{table}
