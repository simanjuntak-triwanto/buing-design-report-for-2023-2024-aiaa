\chapter{Performance} % Main chapter title
\label{ch:performance} % for referencing use \ref{ChapterX}

\section{Mission Profile} %FIX SELESAI
\label{sec:mission-profile}

The mission profile of BUING, as shown in the attached Fig.~\ref{fig:missionProfile}, is designed for cargo aircraft and prioritizes large and heavy payloads. According to the Request for Proposal (RFP), these payloads consist of up to 330 passengers, 48 463L pallets, and three M1A2 Abrams Main Battle Tanks. The request for proposals outlines three distinct mission profiles: maximum payload of 430,000 lb (195,045 kg), transport payloads up to 295,000 lb (133,810 kg), and ferry mission.

Under normal operating conditions, BUING aircraft cruise at Mach 0.75 and climb to its cruising altitude of 31,000 feet (9,448.8 m). The aircraft then begins its descend to an alternate elevation of 15,000 feet (4,572 m) in preparation for a 100 nm (185 km) reserves cruise. If an instrument approach is required, a 45-minute loiter phase is carried out at an altitude of 5,000 ft (1,524 m) prior to landing.

This meticulously outlined mission protocol adheres to stringent operational standards and underscores BUING's capacity to fulfill diverse cargo transport requirements efficiently and reliably. The ratio $\left(\frac{w_{5}}{w_{4}}\right)$ in Table~\ref{tab:weight_ratios_mission_profile} is the residual fuel fraction after the fuel for other segments are firstly allocated and can be calculated using Eq.~\ref{eq:w5_w4}. This residual is the fuel portion used to calculate the range as given in the Segment 4 to 5 in the nominal mission profile given by Fig.~\ref{fig:missionProfile}.

\begin{table}[ht!]
  \centering
  \caption{Weight ratio for each of the segments of the mission profile. The assumed values of the weight ratio are taken from \protect\cite{roskamAirplaneDesign2002}--- military transport aircraft. The cruising to alternate and the loitering weight ratios are calculated using the Breguet formula for jet aircraft \protect\cite{ruijgrokElementsAirplanePerformance2009a}.}
	\begin{tabular}{c||l||c||l}
		\toprule
		No & Mission Segment           & Symbol                               & Weight Ratio       \\
		\midrule
		1  & Engine, Start,  Warm-up   & $\left(\frac{w_{1}}{w_{0}}\right)$   & 0.990              \\
		2  & Taxi                      & $\left(\frac{w_{2}}{w_{1}}\right)$   & 0.990              \\
		3  & Takeoff                   & $\left(\frac{w_{3}}{w_{2}}\right)$   & 0.995              \\
		4  & Climb                     & $\left(\frac{w_{4}}{w_{3}}\right)$   & 0.980              \\
		5  & Cruising                  & $\left(\frac{w_{5}}{w_{4}}\right)$   & Residual           \\
		6  & Descent (attempt to land) & $\left(\frac{w_{6}}{w_{5}}\right)$   & 0.990              \\
		7  & Re-climb                  & $\left(\frac{w_{7}}{w_{6}}\right)$   & 0.980              \\
		8  & Cruising to alternate     & $\left(\frac{w_{8}}{w_{7}}\right)$   & $100~\mathrm{nmi}$ \\
		9  & Loitering                 & $\left(\frac{w_{9}}{w_{8}}\right)$   & 45 minutes         \\
		10 & Descent                   & $\left(\frac{w_{10}}{w_{9}}\right)$  & 0.990              \\
		11 & Landing, Taxi, ShutDown   & $\left(\frac{w_{11}}{w_{10}}\right)$ & 0.992              \\
		\bottomrule
	\end{tabular}
    \label{tab:weight_ratios_mission_profile}
\end{table}

\begin{equation}
  \label{eq:w5_w4}
  \dfrac{w_{5}}{w_{4}} = \dfrac{\dfrac{w_{11}}{w_{10}}}{\dfrac{w_{1}}{w_{0}}\dfrac{w_{2}}{w_{1}}\dfrac{w_{3}}{w_{2}}\dfrac{w_{4}}{w_{3}}\dfrac{w_{6}}{w_{5}}\dfrac{w_{7}}{w_{6}}\dfrac{w_{8}}{w_{7}}\dfrac{w_{9}}{w_{8}}\dfrac{w_{10}}{w_{9}}\dfrac{w_{11}}{w_{10}}}
\end{equation}

\subsection{Maximum Payload} %FIX SELESAI
\label{sec:maximum-payload}

As seen in Fig.~\ref{fig:missionProfile}, BUING's mission profile is designed to comply with the requirements of cargo aircraft operations. The main mission profile described in the RFP requires an unrefueled range of at least 2,500 nm (4,630 km) on internal fuel, reserves included. The operational parameters of BUING meet and even surpass these requirements. BUING is designed to go 3,088 nm (5,720 km) at a speed 0.75 Mach while cruising at an altitude of 31,000 ft (9,448.8 m). This capability exceeds the minimal specifications outlined in the RFP, demonstrating BUING's suitability for long-haul operations.

\subsection{Payload at 295,000 lb} %FIX SELESAI
\label{sec:specific-payload}

Another performance that BUING need to comply based on the RFP is that, being able to reach a maximum range of 5,000 nm (9,260 km) plus reserves with a payload capacity of 295,000 lb (133,810 kg).

\subsection{Ferry Mission} %FIX SELESAI
\label{sec:ferry-mission}

The required mission profile for BUING requires that a ferry mission must have a minimum range of 8,000 nm (14,816 km) without refueling. Surprisingly BUING exceeds this requirement due to the integration of four Rolls Royce Trent 100 engines, achieving an unrefueled range of 8,107 nm (15,014 km).

\section{Lift and Drag Polar}

\begin{table}[ht!]
	\centering
	\caption{Parabolic lift-drag polar of BUING.}
	\begin{tabular}{l||c||c}
		\toprule
		Configuration & $C_{D_0}$ & $\mathrm{k}$ \\
		\midrule
		Cruising      & 0.01941   & 0.04190      \\
		Takeoff       & 0.06117   & 0.04415      \\
		Landing       & 0.05957   & 0.04028      \\
		\bottomrule
	\end{tabular}
    \label{tab:parabolic-lift-drag-polar}
\end{table}

Coefficient lift $C_{L}$ in Chapter~\ref{ch:aerodynamics} shows the lift and drag polar of BUING. Three configurations can be found in the referred figure that represent the aircraft: the first is for cruising, where $\delta_{f}=0^{o}$ and $\delta_{s}=0^{o}$; the second is for take-off, where $\delta_{f}=30^{o}$ and $\delta_{fs}=15^{o}$; and the third is for landing, where $\delta_{f}=40^{o}$ and $\delta_{s}=15^{o}$, each configuration with a different $\mathrm{k}$ and $C_{D_0}$.

Despite the similarity of the plot, as shown in Fig.~\ref{fig:POLAR_DRAG} each describe different aspects of aircraft performance. The first one is $\left(\frac{C_{L}}{C_{D}}\right)$ ratio shows the efficiency at which an aircraft may continue to fly while using the least amount of fuel, which is an indication of the maximum endurance of the aircraft. secondly, the $\left(\frac{C_{L}^3}{C_{D}^2}\right)$ ratio corresponds to the least power needed, which is crucial for maximizing the performance of the aircraft during low-speed activities like takeoff and landing. Finally, the $\left(\frac{C_{L}}{C_{D}^2}\right)$ graph is essential for determining the aircraft's maximum range and proving that it can continue to fly for extended periods of time while using the least amount of fuel. As a result, every graph has a distinct analytical function and provides information on various aspects of aircraft performance and aerodynamic efficiency.

An aircraft's aerodynamic performance can be differentiated by comparing configurations at different $\delta_{f}$ and $\delta_{s}$ settings. The analysis indicates that the configuration with $\delta_{f}=0^{o}$ and $\delta_{s}=0^{o}$, known as the clean or cruising configuration, has the steepest slope with respect to the $\left(\frac{C_{L}}{C_{D}^2}\right)$ ratio. In terms of performance efficiency, this configuration is followed by the landing configuration ($\delta_{f}=40^{o}$ and $\delta_{s}=15^{o}$) and the takeoff configuration ($\delta_{f}=30^{o}$ and $\delta_{fs}=15^{o}$).

The main difference between these plot is how they affect the $\left(\frac{C_{L}}{C_{D^2}}\right)$ ratio when the aircraft is in cruise, which is directly related to its maximum range. Better aerodynamic efficiency is indicated by the clean configuration's noticeable slope rise, which is essential for improving the aircraft's range. The excellent lift-to-drag characteristics, which are essential for extended flight operations, are the source of this efficiency.
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{Performance/lift-drag-polar-plots-buing.pdf}
    \caption{Aerodynamics coefficient ratios.}
    \label{fig:POLAR_DRAG}
\end{figure}

\section{Payload Range} %FIX UDAH SELESAI
\label{sec:payload-range}

The relationship between the BUING aircraft's payload (kg) and operational range (km) at both the 550,453 kg Maximum Takeoff Weight (MTOW) and the 205,453 kg Extended Maximum Takeoff Weight (EMTOW),is shown in Fig.~\ref{fig:payload_curve}. The maximum payload that the aircraft can accommodate is 195,000 kg, when carrying a full payload, the airplane may travel a maximum of 5,720 km from point A to point B (red line). Then, the section from point B to point C (purple line) shows a decrease in payload and an increase in the overall fuel load of the aircraft, allowing the aircraft to reach a range of 12,000 km. Lastly, the area between points C and D (green line) represents the ferry range, which is 15,014 km for an airplane with a maximum fuel load of 234,455 kg.

\begin{figure}[ht!]
    \centering
    \includegraphics[scale=0.4]{Performance/payload_range_jet_curve_buing.pdf}
    \caption{Payload range.}
    \label{fig:payload_curve}
\end{figure}

\section{Airfield Performance}

Airfield Performance is referring to the operating capabilities of fixed-wing and helicopter aircraft, with an emphasis on their effectiveness and economic viability in a range of airfield conditions. This section provides an overview of the airfield performance of the BUING aircraft, highlighting three important factors: Balanced Field Length (BFL) Take-off Distance, and Landing Distance \cite{raymerAircraftDesignConceptual2018}. These parameters are analyzed at various elevations in relation to sea level in order to offer a comprehensive plot of the operational flexibility and limitations of the aircraft.

\subsection{Balance Field Length} %FIX SELESAI

\begin{figure}[!h]
    \begin{minipage}[c]{0.5\linewidth}
        \centering
        \includegraphics[scale=0.3]{Performance/bfl_vs_temp_vs_altitude_buing.pdf}
        \caption{Balanced Field Length (BFL).}
            \label{fig:BFL}
    \end{minipage}\hfill
    \begin{minipage}[c]{0.5\linewidth}
      \centering
      \captionsetup{width=.8\linewidth}
        \includegraphics[scale=0.3]{Performance/takeoff_distance_vs_temp_vs_altitude_buing.pdf}
        \caption{Take-off distance.}
          \label{fig:TAKEOFF}
    \end{minipage}
\end{figure}



The RFP specifies 9,000 feet (2,743 meters) as the Balanced Field Length (BFL) that BUING must meet; however, BUING's BFL is 3,170 meters, which is greater than the RFP.

The given plot also shows operational capability at various airport altitudes, from Sea Level (blue line) to 1,500 meters above sea level (orange line). Multiple lines, indicating different airport elevations relative to sea level and deviation temperatures in Kelvin between $\mathrm{ISA}$ + $\Delta {0}$ and $\mathrm{ISA}$ + $\Delta {20}$, are shown in Fig.~\ref{fig:BFL}. These depictions provide an information of the aircraft's performance characteristics at various temperatures and altitudes.

\subsection{Take-off Distance} %FIX SELESAI

Fig.~\ref{fig:TAKEOFF} provides an explanation of the takeoff distance characteristics of BUING aircraft at various heights and temperature variances. In particular, a takeoff configuration with a $C_{L}$ of 2.5 is used to calculate the takeoff distance for BUING. The plot indicates that, given $\mathrm{ISA}$ + $\Delta {15}$ circumstances, the computed takeoff distance for BUING is 2,611 m at Sea Level. The figure also shows the entire range of takeoff distances for different airport altitudes, from Sea Level to 1500 meters, and includes temperature variations from $\mathrm{ISA}$ + $\Delta {0}$ to $\mathrm{ISA}$ + $\Delta {20}$, respectively.


\subsection{Landing Distance}

Fig.~\ref{fig:LANDING} provides a plot representation of the BUING aircraft's landing distance using a landing configuration with 10$\%$ landing thrust. The landing distance for BUING is 630 meters. Furthermore, the figure illustrates the range of airport heights and the related temperature fluctuations shown in Kelvin units.


\begin{figure}[!h]
    \begin{minipage}[c]{0.5\linewidth}
      \centering
      \captionsetup{width=.8\linewidth}
        \includegraphics[scale=0.3]{Performance/landing_vs_temp_vs_altitude_buing.pdf}
    \caption{Landing distance.}
    \label{fig:LANDING}
    \end{minipage}\hfill
    \begin{minipage}[c]{0.5\linewidth}
        \centering
        \includegraphics[scale=0.3]{Performance/altitude_vs_roc_max_buing.pdf}
    \caption{Service ceiling.}
    \label{fig:SERVICE_CEILING}
    \end{minipage}
  \end{figure}

This plot representation, which takes into consideration important variables including thrust settings and atmospheric circumstances, offers insightful information about the performance characteristics of the BUING aircraft during the landing phase. The observed variations in airport temperatures and elevations highlight how crucial it is to take these factors into consideration when evaluating an aircraft's operational capability in various airfield conditions.

\section{Service Ceiling}
\label{sec:service-ceiling}

Maximum altitude at which an aircraft is able to maintain level flight is known as the ceiling altitude. Because of the lower air density and lower atmospheric pressure at higher altitudes, the airplane can run on less engine power, which saves fuel. This characteristic has a significant impact on fuel efficiency.

The maximum necessary service ceiling for any flight weight is defined in the Request for Proposal (RFP) to be at least 43,000 feet (13,106.40 m). With reference to Fig.~\ref{fig:SERVICE_CEILING}, the BUING aircraft's service ceiling is 33707 ft (10,274 m) at a rate of climb of 0.5 meters per second and 75$\%$ of its maximum take-off weight (MTOW). The graphic provides a thorough picture of the aircraft's performance capabilities at altitude by further outlining distinct service ceilings associated with varying rates of climb.
