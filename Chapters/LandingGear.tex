\chapter{Landing Gear} % Main chapter title
\label{ch:landing-gear} % for referencing, use \ref{ChapterX}

\section{Landing Gear Arrangement and Geometry}
BUING landing gear will use a tricycle arrangement, just like the Lockheed C-5 Galaxy and Antonov An-225 Mriya. This configuration is practically exclusive to this class of aircraft because of its consistent takeoff, taxiing, and payload loading capabilities. This configuration consists of a large nose landing gear at the front of the aircraft and two main landing gear assemblies under the wings toward the rear of the fuselage. As described in Raymer \cite{raymerAircraftDesignConceptual2018}, for aircraft weighing more than 181,440 kg, four bogeys, each with four or six wheels, disperse the entire aircraft load across the runway pavement. It was determined to utilize 12 bogeys, each with 2 wheels, at a design MTOW close to 550,000 kg. This design reduces the pressure on the runway surface during takeoff, landing, and taxiing by distributing the aircraft's weight over a wider area.

\begin{table}[ht!]
	\centering
	\caption{Geometrical parameter of the landing gear.}
	\begin{tabular}{l||r}
		\toprule
		Parameter                       & Value \\ \midrule
		Height (\unit{m})               & 1.5   \\
		Tip back angle (\unit{degree})  & 20.89 \\
		Clearance angle (\unit{degree}) & 16    \\
		Overturn angle (\unit{degree})  & 31.5  \\
		Wheelbase (\unit{m})            & 30.11 \\
		Wheeltrack (\unit{m})           & 7.11  \\ \bottomrule
	\end{tabular}
	\label{fig:parameter_LG}
\end{table}

The landing gear calculations are carried out using the procedures described in Sadrey \cite{sadraeyAircraftDesignSystems2013}. The wheelbase, or the distance between the nose and main landing gears, is selected to meet the stability requirements during taxiing, which state that the nose gear must support between 5 and 20$\%$ of the entire weight. As explained in detail in \cite{sadraeyAircraftDesignSystems2013}, the angle of the tip back, given in Fig.~\ref{fig:longi_placement}, must be less than the angle formed by the vertical passing through the main gear and the aircraft's most aft center of gravity. The overturn is maintained above 25° and below 63°, shown in Fig.~\ref{fig:span_placement}. The landing gear geometrical parameters are shown in Table \ref{fig:parameter_LG} and the detail of CAD drawings are open publicly here: \url{https://tinyurl.com/2coujqho}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.8\linewidth]{LandingGear/Figure1.1_LandingGearLongitudinalPlacement.pdf}
	\caption{Landing gear longitudinal placement.}
	\label{fig:longi_placement}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.8\linewidth]{LandingGear/Figure1.2_LandingGearSpanwisePlacement.pdf}
	\caption{Landing gear span-wise placement.}
	\label{fig:span_placement}
\end{figure}

Recalling the unique design of the Antonov An-225 Mriya, the aircraft lowers its nose gear allowing for easier loading of cargo. The aircraft's front can lower into a stable position, making loading easier, by retracting the nose gear. During this operation, extra support struts are deployed to provide the necessary stability and guarantee the plane stays balanced. The only parts of the airplane keeping it suspended are these support struts when the nose gear is fully retracted and lifted off the ground. At this point, the aircraft has a cargo ramp that extends from the front, making it easy to load big, heavy objects. This Mriya-inspired system makes it possible to handle cargo effectively without sacrificing the stability of the airplane. Nose gear retraction and support struts for cargo loading are given in Fig.~\ref{fig:support_cargo}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.5\linewidth]{LandingGear/Figure1.4SupportStrutsforCargoLoading.pdf}
	\caption{Support struts for cargo loading.}
	\label{fig:support_cargo}
\end{figure}

\section{Tire and Wheel Sizing}
During takeoff and landing procedures, the tires play a vital role in bearing the aircraft's heavy weight. Tire selection must be done carefully in order to ensure that the tires can support the heavy loads encountered during these flying phases, preventing collapse and potentially catastrophic consequences.

\begin{table}[ht!]
	\centering
	\caption{Static loads of each landing gear.}
	\begin{tabular}{l||c||c}
		\toprule
		Parameter                  & Nose                            & Main       \\ \midrule
		MTOW (\unit{Newton})         & \multicolumn{2}{c}{$5,399,944$}              \\
		Static Load (\%)           & 12.5                            & 87.5       \\
		Max Static Load (\unit{N}) & 674992.99                       & 4806344.76 \\
		Min Static Load (\unit{N}) & 593599.17                       & 4724950.94 \\
		\bottomrule
	\end{tabular}
	\label{fig:static_load}
\end{table}

Tire load calculations are usually performed using established methods, as those described in Raymer \cite{raymerAircraftDesignConceptual2018}. The maximum weights that the tires must withstand are calculated using many parameters, including aircraft weight, wheel base, and wheel load geometry. Maximum and minimum static load of each gear are given in Table~\ref{fig:static_load}. The tires are selected by applying the methodology described in \cite{raymerAircraftDesignConceptual2018}, with information obtained from \url{https://tinyurl.com/29vk9b62}. You can locate them in Table~\ref{fig:tire_sizing}. Also stated in the \cite{raymerAircraftDesignConceptual2018}, tire sizes from similar designs can be replicated for early conceptual design, or a statistical method can be applied.

\begin{table}[ht!]
	\centering
	\caption{Tire and wheel sizing.}
	\begin{tabular}{l||r||r}
		\toprule
		Parameter                  & Nose     & Main     \\ \midrule
		Tire Width (\unit{mm})     & 375.412  & 456.184  \\
		Tire Diameter (\unit{mm})  & 1068.832 & 1214.374 \\
		Wheel Diameter (\unit{mm}) & 508      & 660.4    \\ \bottomrule
	\end{tabular}
	\label{fig:tire_sizing}
\end{table}




\section{Shock Absorber}
During landing operations, the landing gears are subjected to significant stresses and vibrations. Strong shock absorbers with the ability to absorb and release energy are essential for controlling these disruptions and reducing oscillations and deformations.

BUING will use an advanced landing gear system with oleo-pneumatic shock absorbers for both the main and nose landing gear components, taking suggestions from the highly regarded C-5 Galaxy and Antonov An-225 Mriya design. Pneumatic spring systems and hydraulic dampening mechanisms are combined in this tried-and-true configuration to provide excellent performance in a variety of operating environments. It aims to provide consistent and dependable performance throughout every phase of flight operations by utilizing this tried-and-true design.

\begin{table}[ht!]
	\centering
	\caption{Oleo outer diameter.}
	\begin{tabular}{l||r}
		\toprule
		Landing Gear & Diameter (\unit{mm}) \\ \midrule
		Nose         & 291.592              \\
		Main         & 252.476              \\ \bottomrule
	\end{tabular}
	\label{fig:oleo_diameter}
\end{table}

Stated in Raymer's Section 11.4.3, the internal pressure of compressed air applied across a piston allows the oleo to support its load. An oleo's internal pressure P is typically 1800 psi, or 12,415 kPa. The equation force = pressure $\times$ area can be used to calculate internal diameter. Since the external diameter is usually 30$\%$ bigger than the piston diameter, Raymer's Eq. 11.13 can be used to approximate the external oleo diameter. Table \ref{fig:oleo_diameter} provides the calculated external diameters of the nose and main gear oleo.

\section{Gear Retraction and Storage}
With the Antonov An-225 Mriya as guidance, the landing gear system's retraction and storage mechanisms have been carefully considered in order to achieve maximum performance and safety under a variety of operating circumstances.

\subsection{Nose Gear Retraction}
The aircraft's nose gear retracts in the direction of the rear, as shown in Fig.~\ref{fig:nose_storage}. Comparing this retraction direction to forward retraction, there are a number of benefits. A simpler mechanism is usually needed when the nose gear retracts backward, which can improve reliability and make maintenance simpler. Aerodynamically speaking, backward retraction causes less disruption to the airflow, which may increase fuel efficiency while in flight. Because the gear retracts backward and away from the impact direction, it also helps to lessen the chance of shearing during landing. Furthermore, the aircraft achieves better weight distribution and a more balanced center of gravity by retracting the gear toward the rear, which can improve flying stability.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.5\linewidth]{LandingGear/Figure4.1_NoseGearStorage.pdf}
	\caption{Nose gear storage.}
	\label{fig:nose_storage}
\end{figure}

\subsection{Main Gear Retraction}
The main landing gear retracts inside the aircraft's fuselage, shown in Fig.~\ref{fig:main_storage}, attempting the design of the landing gear on the An-225 Mriya. The main landing gear retracts by rotating around its connection points and folding inward toward the fuselage's midline. This arrangement helps to maintain the aircraft's aerodynamic cleanliness while in flight by making the most use of the space inside the landing gear bays and making it easier to store the landing gear inside the aircraft's structure.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.5\linewidth]{LandingGear/Figure4.2_MainGearStorage.pdf}
	\caption{Main gear storage.}
	\label{fig:main_storage}
\end{figure}

The goal is to achieve robustness, dependability, and aviation standard compliance in the landing gear retraction and storage systems by implementing these design ideas from the An-225 Mriya landing gear system. Depicted in Fig.~\ref{fig:sequence} is the landing gear retraction sequence.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{LandingGear/Figure4.3_LandingGearRetractionSequence.pdf}
	\caption{Landing gear retraction sequence.}
	\label{fig:sequence}
\end{figure}
