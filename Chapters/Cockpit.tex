\chapter{Cockpit and Avionics} % Main chapter title
\label{ch:cockpit} % for referencing, use \ref{ChapterX}

%------------------------------------------------------------------------
\section{Cockpit Arrangement}
\label{sec:cockpit-arrangement}
%------------------------------------------------------------------------

Based on Roskam's \cite{roskamAirplaneDesign2002} and Gudmundsson's \cite{gudmundssonGeneralAviationAircraft2022} works, the BUING cockpit layout takes ergonomics and human aspects into account. Its layout, intended for use on bomber and cargo aircraft, maximizes the performance of the pilot, co-pilot, and crew over extended flight duration. Reference eye point is used to calculate sitting inclinations and measure distances from reference eye point to flight control and instrument panel. The cockpit arrangement is depicted in Fig.~\ref{fig:cockpit_arrangement}. The CAD drawings are open publicly at \url{https://tinyurl.com/24koyp77}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\linewidth]{Cockpit/cockpit-arrangement-cropped.pdf}
	\caption{Cockpit arrangement.}
	\label{fig:cockpit_arrangement}
\end{figure}

%------------------------------------------------------------------------
\section{Seat Arrangement}
%------------------------------------------------------------------------

Seat design and distances to the flight control and instrument panel are also based on the works of Roskam \cite{roskamAirplaneDesign2002} and Sadrey \cite{sadraeyAircraftDesignSystems2013}. By applying the ergonomic cockpit arrangement, BUING intended to support the crew’s work by reducing unnecessary movement to reach the control or instrument panel. The reference eye point is used as the starting point for drawing lines and calculating distances to the instrument panel, rudder pedal, and middle joystick of the flight control system. The configuration of the seats and controls is inspired by the works of Roskam. Figs.~\ref{fig:seat_control}, \ref{fig:seat_distance} $\&$ \ref{fig:seat_width}, respectively, shows the layout of the instrument panel and seat control as well as the distance and space between the seats. .
\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.95\linewidth]{Cockpit/Seat-to-control-and-instrument-panel-arrangement-cropped.pdf}
	\caption{Seat to flight control and instrument panel arrangement in millimetre; (1) pilot eye position, (2) neutral seat reference point, (3) yoke reference point arc.}
	\label{fig:seat_control}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\linewidth]{Cockpit/seat_distance.pdf}
	\caption{Seat distance (in \unit{mm}).}
	\label{fig:seat_distance}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1\linewidth]{Cockpit/seat-width.pdf}
	\caption{Seat width (in \unit{mm}).}
	\label{fig:seat_width}
\end{figure}

%------------------------------------------------------------------------
\section{Crew's Visibility Optimization}
%------------------------------------------------------------------------

Assuring the crews have unobstructed vision following Visual Flight Rules (VFR) allows them to keep an ideal viewpoint of the aircraft's surroundings from both a vertical (downward and upward) and horizontal (port and starboard) standpoint. This visibility is essential for the pilot to be able to see the ground path during important stages including takeoff, landing, and taxiing referring on Airworthiness Standard: AS 580 B. Optimized BUING visibility Pattern is shown in Fig.~\ref{fig:visibility}. The method used is by drawing manual angles through Onshape.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.75\linewidth]{Cockpit/Optimised-Visibility-Pattern-from-Port-Side.pdf}
	\caption{Optimised visibility pattern from port side.}
	\label{fig:visibility}
\end{figure}

%------------------------------------------------------------------------
\section{Flight Crew’s Outside Visibility}
%------------------------------------------------------------------------


As stated in \cite{gudmundssonGeneralAviationAircraft2022} regarding the ideal pilot field of view, optimizing the flight crew's exterior visibility is crucial for safe operations, especially during crucial stages such as takeoff, taxiing, and landing. The vertical side view visibility is shown in Fig.~\ref{fig:side_visibility}, and horizontal view in Fig.~\ref{fig:horizontal_view}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Cockpit/Upward-and-Downward-Forward-Side-Visibility-cropped}
	\caption{Side and forward vertical visibility.}
	\label{fig:side_visibility}
\end{figure}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=1.0\linewidth]{Cockpit/Horizontal-Side-View.pdf}
	\caption{Horizontal side view.}
	\label{fig:horizontal_view}
\end{figure}

%------------------------------------------------------------------------
\section{Avionics Integration}
\label{sec:avionics-integration}
%------------------------------------------------------------------------

To reduce flight crew fatigue, BUING employs an Integrated Avionics System with a glass cockpit. To keep the heavy-lift aircraft airworthy throughout the duration of the objectives, BUING integrates conventional avionics with both military and civil avionics. The proposed Avionic system are shown in Tables \ref{fig:avionic_3}, \ref{fig:avionic_4}, \ref{fig:avionic_1}, \ref{fig:avionic_2}. The selection of the flight management system and the flight control system are expected to support autopilot operations.

\begin{table}[H]
	\caption{Avionics list: Part 1}
	\begin{tabular}{l||l||l}
		\toprule
		Required System                & Product                                    & Brand                               \\ \midrule
		Weather radar                  & \multirow{5}{*}{ Flight2 avionics system } & \multirow{5}{*}{ Collinsaerospace } \\
		GPS / Navigation Systems       &                                            &                                     \\
		INS                            &                                            &                                     \\
		TACAN                          &                                            &                                     \\
		Flight Management              &                                            &                                     \\ \midrule
		Flight Control System          & \begin{tabular}{l}
			                                 FCS-7000 Flight Control \\
			                                 system
		                                 \end{tabular}                 & Collinsaerospace                                 \\ \midrule
		Multi functional display (LCD) & \begin{tabular}{l}
			                                 MFD-4820 Large Area \\
			                                 Display
		                                 \end{tabular}                     & Collinsaerospace                             \\ \midrule
		Head Up Display                & LiteWave ${ }^{\circledR}$ Head-Up Display & BAE Systems                         \\ \midrule
		Terrain radar                  & AN/APN-209                                 & Raytheon                            \\ \midrule
		\begin{tabular}{l}
			VHF/UHF Radio Transceiver, \\
			Communication Systems
		\end{tabular}  & AN/ARC-210 radio system                    & Collinsaerospace                                    \\ \midrule
		\bottomrule
	\end{tabular}
	\label{fig:avionic_3}
\end{table}



\begin{table}[H]
	\caption{Avionics list: Part 2}
	\begin{tabular}{l||l||l}
		\toprule
		IFF Transponder                                      & \begin{tabular}{l}
			                                                       AN/APX-119 Identification \\
			                                                       Friend or Foe (IFF) transponder
		                                                       \end{tabular}  & Raytheon                                \\ \midrule
		SATCOM                                               & \begin{tabular}{l}
			                                                       IRT NX SATCOM System for \\
			                                                       Iridium $®$
		                                                       \end{tabular}      & Collinsaerospace                         \\ \midrule
		\multirow{2}{*}{ Video Cameras }                     & \begin{tabular}{l}
			                                                       Taxi-Aid and Landscape \\
			                                                       Camera System
		                                                       \end{tabular}        & \multirow{2}{*}{ Collinsaerospace }    \\ \midrule
		                                                     & \begin{tabular}{l}
			                                                       Cabin Video Monitoring \\
			                                                       System (CVMS)
		                                                       \end{tabular}        &                                        \\ \midrule
		\begin{tabular}{l}
			Radar/Threat warning system,      \\
			Electronic Support and Protection \\
			Systems
		\end{tabular}                 & \begin{tabular}{l}
			                                AN/ALQ-214 Integrated   \\
			                                Defensive Electronic    \\
			                                Countermeasures (IDECM) \\
			                                system
		                                \end{tabular}       & L3HARRIS                                                       \\ \midrule
		\multirow[b]{3}{*}{ Weapon/Countermeasures Systems } & \begin{tabular}{l}
			                                                       AN/ALQ-131 Electronic \\
			                                                       Countermeasures (ECM) Pod
		                                                       \end{tabular}        & \multirow[b]{3}{*}{ Northrop Grumman } \\
		                                                     & \begin{tabular}{l}
			                                                       AN/AAQ-24(V) DIRCM    \\
			                                                       (Directional Infrared \\
			                                                       Countermeasure)
		                                                       \end{tabular}         &                                       \\ \midrule
		Electro-Optic and Infrared Systems                   & \begin{tabular}{l}
			                                                       LITENING Advanced \\
			                                                       Targeting Pod
		                                                       \end{tabular}             & Northrop Grumman                  \\ \midrule
		\begin{tabular}{l}
			Load Planning and Management \\
			Systems
		\end{tabular}                      & \begin{tabular}{l}
			                                     Advanced Cargo Loading and \\
			                                     Delivery System (ACLADS)
		                                     \end{tabular}    & Collinsaerospace                                             \\ \midrule
		Electronic Publications Bag                          & Electronic Cabin Bag (ECB)       & Collinsaerospace           \\ \midrule
		\begin{tabular}{l}
			De-Icing/Anti-Icing Systems \\
			component
		\end{tabular}                       & \begin{tabular}{l}
			                                      Vibrating probe ice detectors \\
			                                      and magnetostrictive ice      \\
			                                      detectors (MID)
		                                      \end{tabular} & Collinsaerospace                                               \\ \midrule
		APU                                                  & HGT-1700                         & Honeywell                  \\
		\bottomrule
	\end{tabular}
	\label{fig:avionic_4}
\end{table}

\begin{sidewaystable}[ht!]
	\centering
	\caption{Avionics list: part 3}
	\includegraphics[width=1\linewidth]{Cockpit/Avionics-List-Part1.pdf}
	\label{fig:avionic_1}
\end{sidewaystable}


\begin{sidewaystable}[ht!]
	\centering
	\caption{Avionics list: part 4}
	\includegraphics[width=1\linewidth]{Cockpit/Avionics-List-Part2.pdf}
	\label{fig:avionic_2}
\end{sidewaystable}
