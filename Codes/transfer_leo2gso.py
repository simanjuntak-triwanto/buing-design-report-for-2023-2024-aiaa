#!/usr/bin/env python3
import numpy as np
from izzo_lib import lambert_izzo, lambert_univ
from jgm2_constants import MIU_N, ER, VU, TU
from keplerian_elements import randv, elorb
from maneuvers import hohman_trans
from numpy.linalg import norm
import matplotlib.pyplot as plt


def semi_p(a, e):
    res = a * (1 - e ** 2)

    return res


# Initial orbit
e0 = 0
h0 = 220  # altitude (km)
a0 = (ER + h0) / ER
p0 = semi_p(a0, e0)
inc0 = 0
Omega0 = 0
omega0 = 0
nu0 = 0
coe0 = [p0, e0, inc0, Omega0, omega0, nu0]
r0, v0 = randv(coe0)


# Final orbit
e1 = 0
h1 = 35786  # altitude (km)
a1 = (ER + h1) / ER
p1 = semi_p(a1, e1)
inc1 = 0
Omega1 = 0
omega1 = 0
nu1 = 180
coe1 = [p1, e1, inc1, Omega1, omega1, nu1]
r1, v1 = randv(coe1)


r0_ = norm(r0)
r1_ = norm(r1)

a_trans, tau_trans, Dva, Dvb = hohman_trans(r0_, r1_)

tofs = np.linspace(0.7 * tau_trans, 10 * tau_trans, 500)

Dvs = np.array([])
for tof in tofs:
    ((v0_lam, v1_lam),) = lambert_izzo(r0, r1, tof=tof, M=0,)
    # v0_lam, v1_lam = lambert_univ(r0, r1, tof=tof, short=True,)
    Dv0 = norm(v0_lam - v0)
    Dv1 = norm(v1_lam - v1)
    Dv = Dv0 + Dv1
    Dvs = np.append(Dvs, Dv)

fig, ax = plt.subplots(figsize=(9, 9))
ax.plot(tofs * TU / 3600, Dvs * VU, "o")
ax.set_xlabel("tof (hour)")
ax.set_ylabel("DV (km/s)")
plt.grid("both")
plt.show()
